import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'breadcrumb_.login', // 直接点出语言包中对应的文字
    component: () => import('../views/Login.vue')
  },
  {
    path: '/home',
    name: 'breadcrumb_.home', // en.js中的属性
    component: Home
  },
  {
    path: '/myprofile',
    name: 'breadcrumb_.myprofile',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/MyProfile.vue')
    }
  },
  {
    path: '/manage',
    name: 'breadcrumb_.manage', // 直接点出语言包中对应的文字
    component: () => import('../views/Manage.vue')
  },
  {
    path: '/findpasw',
    name: 'breadcrumb_.findpasw',
    component: () => import('../views/FindPasw.vue')
  },
  {
    path: '/charge',
    name: 'breadcrumb_.charge',
    component: () => import('../views/student/Charge.vue')
  },
  {
    path: '/paySuccess',
    name: 'breadcrumb_.paySuccess',
    component: () => import('../views/student/PaySuccess.vue')
  },
  {
    path: '/queryAllUser',
    name: 'breadcrumb_.queryAllUser',
    component: () => import('../views/teacher/QueryAllUserInfo.vue')
  },
  {
    path: '/department',
    name: 'breadcrumb_.department',
    component: () => import('../views/teacher/Department.vue')
  },
  {
    path: '/classmanage',
    name: 'breadcrumb_.classmanage',
    component: () => import('../views/teacher/ClassManage.vue')
  },
  {
    path: '/sftp',
    name: 'breadcrumb_.sftp',
    component: () => import('../views/teacher/SftpManage.vue')
  },
  {
    path: '/manage_auth',
    name: 'breadcrumb_.authManage',
    component: () => import('../views/teacher/AuthorityManage.vue')
  }
]

const router = new VueRouter({
  mode: 'history',  // 默认是hash模式，路径后边会带/#/号，所以需要改成history模式。改成history会出现空白页，需要后端nginx部署配合
  // https://router.vuejs.org/zh/guide/essentials/history-mode.html#后端配置例子
  base: process.env.BASE_URL,
  // base: './',
  routes
})

// 设置路由守卫，如果用户在访问前，没有得到这个响应头，让其跳转到登录页重新登录
router.beforeEach((to, from, next) => {
  if (to.path === '' || to.path === '/' || to.path === '/findpasw') {
    // console.log(to.path)
    next(); // 放行
  } else {
    let token = localStorage.getItem('Authorization');
    // console.log('route.js')
    // console.log(token)
    if (token === 'null' || token === '' || token === null) {
      next('/');
      // this.$router.push({ path: "/login" });
      // next(false) 阻止跳转 中断导航
    } else {
      next();
    }
  }
});

export default router
