import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios' //引入axios
import ElementUI from 'element-ui'; //引入饿了么JS模块
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css'; //引入饿了么CSS

// js bootstrap proper.js slimscroll
import $ from 'jquery'
window.$ = $
window.jQuery = $
export default $
// import 'bootstrap/dist/css/bootstrap.min.css'
// import 'bootstrap/dist/js/bootstrap.min'

// 引用自己封装的组件
// 引用自己封装的组件
import domtool from './utils/domtool'
Vue.prototype.$domtool = domtool

// 对MD5的支持
import md5 from 'js-md5';
Vue.prototype.$md5 = md5;

axios.defaults.withCredentials = true;
Vue.prototype.$http = axios //声明axios并且起别名$http，等同于 Vue.use(axios)
import qs from 'qs' //引入qs模块，用来序列化post类型的数据，某些请求会用得到
Vue.use(ElementUI, { locale  }) //声明饿了么模块

import VueI18n from 'vue-i18n'
import enLocale from 'element-ui/lib/locale/lang/en'        //引入Element UI的英文包
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'     //引入Element UI的中文包
Vue.use(ElementUI,{
  i18n: (key, value) => i18n.t(key, value)
});
Vue.use(VueI18n) // 通过插件的形式挂载
 
const i18n = new VueI18n({
  locale: 'zh',    // 语言标识
  fallbackLocale: 'zh',//没有英文的时候默认中文语言
  silentFallbackWarn: true,//抑制警告
  //this.$i18n.locale // 通过切换locale的值来实现语言切换
  messages: {
  'zh': Object.assign(require('./assets/common/lang/zh'),zhLocale),   // 中文语言包数组
  'en': Object.assign(require('./assets/common/lang/en'),enLocale)    // 英文语言包数组
  }
})

// 关闭配置提示
Vue.config.productionTip = false

// 添加请求拦截器   发生在请求之前
axios.interceptors.request.use(config => {
  // 如果登录获得的响应头Token还在本地存储中，就把它加到下一次请求的请求头中,不需要清除，若token过期返回401,转向登录页即可
  let token = localStorage.getItem('Authorization')
  if (token != 'null' || token != '' || token != null) {
    config.headers.Identifier = token;
    // config.headers['Authorization'] = 
  }
  // 如果不是post请求,不做处理
  if (config.method != 'post') {
      // 是post以外的请求   ||  如get传递的参数时formData
      return config
  }
  // 对于POST请求自定义请求头的，如下，则不自动转化为json
  if ( config.headers['Content-Type'] === 'multipart/form-data' ){
      return config
  }
  /**
   * 由于使用Restful风格的接口，即增删查改的接口名都为同一个，如dept
   *     方法以POST(增)、GET(查)、PUT(改)、DELETE(删)来区分
   * 但是dept新增接口用的是params类型的参数，而不是body(json)类型的参数，所以想方法略过这个转换
   * 以下拦截器会对POST做处理，现在自定义一个请求头绕过这个JSON.stringify(config.data)
   */
  if ( config.headers['Post-Transform'] === 'neglect' ){
    return config
  }
  // 把数据进行转换
  /**
   * qs.stringify()将对象序列化成URL的形式，以&进行拼接。
   * JSON.stringify()讲对象转换为json字符串
   */
  // qs.stringify(config.data)
  config.data = JSON.stringify(config.data)
  return config;
}, function(error) {
  // 对请求错误做些什么 TODO
  return Promise.reject(error);
});

// 添加响应拦截器    发生在后台给我返回请求之前
axios.interceptors.response.use(response => {
  // 对响应数据做点什么 TODO
  return response;
}, error => {
  console.log('error')
  console.log(error)
  // 对响应错误做点什么 TODO
  /**
   *对响应错误做点什么
    服务器状态码不是2开头的的情况
    这里可以跟你们的后台开发人员协商好统一的错误状态码
    然后根据返回的状态码进行一些操作，例如登录过期提示，错误提示等等
    下面列举几个常见的操作，其他需求可自行扩展
   */
  if (error.response.status) {
    switch (error.response.status) {
      // 401: 未登录
      // 未登录则跳转登录页面，并携带当前页面的路径
      // 在登录成功后返回当前页面，这一步需要在登录页操作。
      case 401:
        // 如果前端拿到状态码为401，就清除token信息并跳转到登录页面
        localStorage.removeItem('Authorization');
        this.$router.push('/login');
        break;
      case 403:
        // TODO
        break;
      case 404:
        // TODO
        break;
      default:
        // TODO
        break;
    }
    return Promise.reject(error.response);
  }
  return Promise.reject(error);
});

new Vue({
  router,
  store,
  i18n,
  render: function (h) { return h(App) }
}).$mount('#app')
