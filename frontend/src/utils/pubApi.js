import axios from 'axios'
import qs from 'qs'
// 主机域名
const baseUrl = 'http://127.0.0.1:9096/edu'
// redis功能url
let baseUrlQoute = 'http://127.0.0.1:9096/edu/redis'
// 查询单个redisKey的值
export const getKeyValue = params => {
    return axios.get(`${baseUrl}/redis/rget`, qs.stringify(params))
}
// 遍历redis的Key列表
// let data = {  // params
//     pattern: "*"
// }
export const getKeyList = params => {
    return axios.get(`${baseUrlQoute}/r-ergodic`, params)
}
//获取验证码
export const getCaptcha = params => {
    return axios.get(`${baseUrl}/defaultKaptcha?d` + new Date() * 1, params)
}