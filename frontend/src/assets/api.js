import axios from 'axios';
import qs from 'qs';
let baseUrls = 'http://118.178.85.6:3000'; //正式

// 直播间列表
export const find = params => {
    return axios.get(`${baseUrls}/liveBroadcastingRoom/find`, {
        params: params
    })
}
// 创建直播
export const liveadd = params => {
    return axios.post(`${baseUrls}/liveBroadcastingRoom/add`, qs.stringify(params))
}
// 直播管理删除
export const livedelete = params => {
    return axios.get(`${baseUrls}/liveBroadcastingRoom/delete`, {
        params: params
    })
}
// 直播间根据ID查询
export const findbyid = params => {
    return axios.get(`${baseUrls}/liveBroadcastingRoom/findByID`, {
        params: params
    })
}
// 直播间上线，下线
export const liveroomUpdate = params => {
    return axios.post(`${baseUrls}/liveBroadcastingRoom/update`, qs.stringify(params))
}
// 直播间置顶
export const liveroomupdatePlacement = params => {
    return axios.get(`${baseUrls}/liveBroadcastingRoom/updatePlacement`, {
        params: params
    })
}
// 直播间修改
// export const liveroomUpdate = params => {
//         return axios.post(`${baseUrls}/liveBroadcastingRoom/update`, qs.stringify(params))
//     }
//直播间取消置顶
export const liveroomupdateNotPlacement = params => {
    return axios.get(`${baseUrls}/liveBroadcastingRoom/updateNotPlacement`, {
        params: params
    })
}