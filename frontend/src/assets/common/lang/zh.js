module.exports = {	
	language: {
		name: '中文'
	},
	loginpage: {
		title: '教务管理系统登录',
        username: '账号',
        username_tip: '请输入帐号',
        password: '密码',
        password_tip: '请输入密码',
        checkcode: '验证码',
        checkcode_tip: '验证码',
        login: '登录',
        registry: '注册',
        findpasw: '找回密码',
        status: '身份:',
        status_1: '学生',
        status_2: '教师',
        status_3: '管理'
    },
    breadcrumb_: {
        current_position: '当前位置',
        home: '主页',
        myprofile: '个人信息',
        manage: '管理',
        login: '登录',
        findpasw: '找回密码',
        charge: '账户管理',
        paySuccess: '充值成功',
        queryAllUser: '师生信息查询',
        department: '部门管理',
        classmanage: '班级管理',
        sftp: 'SFTP',
        authManage: '权限管理'
    },
    findpasw: {
        title: '找回密码',
        email: '邮箱',
        email_tip: '请输入邮箱去获取验证码',
        captcha_tip: '请输入验证码',
        get_captcha: '获取验证码',
        newpasw_tip: '请输入新密码',
        reinnewpasw_tip: '新密码确认',
        update_pass: '更新密码',
        to_login: '想起密码？'
    },
    wrapper_header: {
        userType_stu: '学生',
        userType_tea: '教师',
        searchBar_tip: '搜索框，点击按钮清除',
        profile: '个人信息',
        logout: '注销'
    },
    nav_title: {
        stu_base_menu: '学生基础功能菜单',
        tea_base_menu: '职工基础功能菜单',
        tea_course: '任课教师功能菜单',
        tea_class: '班级教师功能菜单',
        tea_speciality: '专业教师功能菜单',
        tea_finance: '财管职工功能菜单',
        adm_menu: '管理员菜单',
    },
    nav: {
        // stu base
        home: '主页',
        myprofile: '个人信息',
        charge: '账户管理',
        myaccount: '我的账户',
        estimate: '教学评价',
        leave: '事假申请',
        score: '成绩管理',
        textbook_order: '教材预定',
        tem_course_select: '活动课程',
        // tea base
        textbook: '教材管理',
        persion_info: '师生信息查询',
        // Teacher of courses
        score_manage: '成绩管理',
        teaching: '在授课程管理',
        // Teacher of class management
        leave_manage: '事假管理',
        // Teacher Leader of this speciality
        course_manage: '课程管理',
        class_course: '班级排课',
        sftp_system: '文件系统',
        estimate_manage: '教学评价管理',
        // Financial Intendant
        txtbook_order_manage: '教材订购管理',
        scholarship_manage: '奖学金管理',
        // admin
        info_manage: '师生信息管理',
        power_manage: '权限管理',
        department: '部门信息管理',
        class_manage: '班级信息管理',
        temp_course: '活动课程管理'
    }
}