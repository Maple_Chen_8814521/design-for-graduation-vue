module.exports = {	
	language: {
        name: 'English'
	},
	loginpage: {
		title: 'Edu Management System',
        username: 'Username',
        password: 'Password',
        username_tip: 'Please Entry Username',
        password_tip: 'Please Entry password',
        checkcode: 'Captcha',
        checkcode_tip: 'Please Entry Captcha',
        login: 'Sign in',
        registry: 'register',
        findpasw: 'Reset password',
        status: 'Role:',
        status_1: 'Student',
        status_2: 'Teacher',
        status_3: 'Superviser'
    },
    breadcrumb_: {
        current_position: 'Current Location',
        home: 'Home',
        myprofile: 'MyProfile',
        manage: 'Manage',
        login: 'Login',
        findpasw: 'FindPasw',
        charge: 'Balance Management',
        paySuccess: 'Charge Successfully',
        queryAllUser: 'Information Query',
        department: 'Department Management',
        classmanage: 'Class Management',
        sftp: 'SFTP',
        authManage: 'Authority Management'
    },
    findpasw: {
        title: 'Find Password Back',
        email: 'Mail',
        email_tip: 'Please Entry email address and to get Captcha',
        captcha_tip: 'Please Entry Captcha',
        get_captcha: 'Get Captcha',
        newpasw_tip: 'Please Entry New Password',
        reinnewpasw_tip: 'Please Confirm New Password',
        update_pass: 'Update Password',
        to_login: 'Sign in'
    },
    wrapper_header: {
        userType_stu: 'Student',
        userType_tea: 'Teacher',
        searchBar_tip: 'Search here, Click btn to Clear',
        profile: 'My Profile',
        logout: 'Sign Out'
    },
    nav_title: {
        stu_base_menu: 'Student Base Menu',
        tea_base_menu: 'Teacher Base Menu',
        tea_course: 'Course Teacher Menu',
        tea_class: 'Class Teacher Menu',
        tea_speciality: 'Speciality Teacher Menu',
        tea_finance: 'Finance Staff Menu',
        adm_menu: 'Administrator\'s Menu',
    },
    nav: {
        // stu base
        home: 'Home Page',
        myprofile: 'Self Profile',
        charge: 'Balance Management',
        myaccount: 'My Account',
        estimate: 'Instructional Evaluation',
        leave: 'Leave Apply',
        score: 'Performance Management',
        textbook_order: 'TextBook Order',
        tem_course_select: 'Temporary Course',
        // tea base
        textbook: 'Textbook Management',
        persion_info: 'Information Query',
        // Teacher of courses
        score_manage: 'Performance management',
        teaching: 'Teaching management',
        // Teacher of class management
        leave_manage: 'Leave Management',
        // Teacher Leader of this speciality
        course_manage: 'Term Courses',
        class_course: 'Arrange Course',
        sftp_system: 'File Management',
        estimate_manage: 'Evaluation Management',
        // Financial Intendant
        txtbook_order_manage: 'TxtBook Order Management',
        scholarship_manage: 'ScholarShip Management',
        // admin
        info_manage: 'Information Management',
        power_manage: 'Authority Management',
        department: 'Department Management',
        class_manage: 'Class Info Management',
        temp_course: 'Temporary Courses'
    }
}