# frontend

## Project setup
```
npm install
# 单独为项目安装element  (https://element.eleme.cn/#/zh-CN/component/popconfirm)
npm i element-ui --save
# 安装 vue 路由模块vue-router和网络请求模块vue-resource
npm i vue-router vue-resource --save
# 装axios请求封装模块
npm install axios --save
# 安装md5
npm install --save js-md5
# 国际化语言包
npm install vue-i18n --save

npm i jquery-slimscroll bootstrap@4 jquery-min -S
npm install --save popper.js
 
/*
import $ from 'jquery'
// import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min'
*/
```

npm安装模块
    【npm install xxx】利用 npm 安装xxx模块到当前命令行所在目录；
    【npm install -g xxx】利用npm安装全局模块xxx；

本地安装时将模块写入package.json中：
    【npm install xxx】安装但不写入package.json；
    【npm install xxx –save】 安装并写入package.json的”dependencies”中；
    【npm install xxx –save-dev】安装并写入package.json的”devDependencies”中。

npm 删除模块
【npm uninstall xxx】删除xxx模块； 
【npm uninstall -g xxx】删除全局模块xxx；

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
