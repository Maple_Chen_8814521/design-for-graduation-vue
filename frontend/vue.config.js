module.exports = {
    publicPath: './', // 项目基本路径。./这样打出来的包可以被部署在任意路径。若定义为/app，只能把dist下的内容丢在根路径下的app文件夹中
    outputDir: 'dist', // 输出文件目录
    assetsDir: 'assets', // 设置放置打包生成的静态资源 (js、css、img、fonts) 的目录。该目录是相对于 outputDir
    parallel: false,
    // devServer用来配置服务器启动的方式
    productionSourceMap: false, // false防止打包的js文件额外加上map扩展名
    devServer: {
        host: "127.0.0.1", //是你的地址,不要使用localhost,会导致请求开启多个会话，即JSESSIONID不一致
        port: 8081, // 端口号
        https: false,
        // open: true, //配置自动启动浏览器
        disableHostCheck: true,  // 允许进行内网穿透
        // 配置多个代理（这个是代理跨域配置）
        proxy: {
            // 可以给你跨域的服务器起名字,虽然浏览器中请求的地址是localhost,但是代理会将该请求转发的
            /**
             * 20210208：后台的地址是x::x:9096/edu/接口名，自然cookie的有效path为/edu，
             * 但是在前端浏览器使用F12看network看到的请求地址却是代理，xxxx:8081/auth/接口名，所以cookie写不上来，每次的JSESSIONID自然会不一致
             */
            "/auth": {
                // target: "http://yztouch.top/auth",
                target: "http://127.0.0.1:8901/auth",  // 部署到服务器时请使用该代理，防止域名更换导致服务无法访问
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    "^/auth": "" // 去掉接口地址中的api字符串
                }
            },
            //可以随便起名字，也可以起多个代理，都可以。
            "/edu": {
                // target: "http://yztouch.top/edu", // 给我写你要跨域的服务器地址
                target: "http://127.0.0.1:8080/edu", // 部署到服务器时请使用该代理，防止域名更换导致服务无法访问
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    "^/edu": "" // 去掉接口地址中的api字符串
                        // 这个地方的名称必须^+你的跨域服务器起的名字
                }
            }
        }
    },
    // 防止打包时出现单个js文件大于244K的警告
    configureWebpack : {
        performance: {
            hints:'warning',
            //入口起点的最大体积 整数类型（以字节为单位）
            maxEntrypointSize: 50000000,
            //生成文件的最大体积 整数类型（以字节为单位 300k）
            maxAssetSize: 30000000,
            //只给出 js 文件的性能提示
            assetFilter: function(assetFilename) {
                return assetFilename.endsWith('.js');
            }
        }
    },
}